package com.javachallenge.todolist.application.repository;

import java.util.List;
import java.util.Optional;

public interface Repository<T, ID> {
    T save(T type);

    List<T> findAll();

    Optional<T> findById(ID id);

    T update(T type);

    void delete(ID id);
}
