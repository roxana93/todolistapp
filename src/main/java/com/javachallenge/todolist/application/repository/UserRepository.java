package com.javachallenge.todolist.application.repository;

import com.javachallenge.todolist.application.model.User;

import java.util.Optional;

public interface UserRepository extends Repository<User, Long> {
    Optional<User> getUserByEmail(String email);
}
