package com.javachallenge.todolist.application.repository.impl;

import com.javachallenge.todolist.application.exception.ResourceNotFoundException;
import com.javachallenge.todolist.application.exception.RetryException;
import com.javachallenge.todolist.application.model.Tasker;
import com.javachallenge.todolist.application.repository.TaskerRepository;
import com.javachallenge.todolist.application.repository.mapper.TaskerMapper;
import com.javachallenge.todolist.application.repository.utils.TaskerQueries;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;

@Repository
@Slf4j
public class TaskerRepositoryImpl extends JdbcDaoSupport implements TaskerRepository {

    @Autowired
    DataSource dataSource;

    @Autowired
    TaskerMapper taskerMapper;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    @Override
    public Tasker save(Tasker task) {
        try {
            getJdbcTemplate().update(TaskerQueries.CREATE_USER, task.getTitle(), task.getStartDate(), task.getEndDate(), task.getDescription(), task.getPriority().getValue(), task.getUser().getUserId());
            return task;
        } catch (CannotGetJdbcConnectionException exception) {
            throw new RetryException("Connection error ", exception);
        }
    }

    @Override
    public List<Tasker> findAll() {
        log.info("Retrieving all tasks");
        List<Tasker> taskList = getJdbcTemplate().query(TaskerQueries.GET_ALL_TASKS, taskerMapper);
        return taskList;
    }

    @Override
    public List<Tasker> findTasksByUserId(Long userId) {
        log.info("Retrieving all tasks for user id ={}", userId);
        return getJdbcTemplate().query(TaskerQueries.GET_TASKS_BY_USER_ID,
                new Object[]{userId}, taskerMapper);
    }

    @Override
    public Optional<Tasker> findTaskByUserIdAndTaskId(Long userId, Long taskId) {
        log.info("Retrieving a specific task for user id={} with task id = {} ", userId, taskId);
        try {
            return Optional.ofNullable(getJdbcTemplate().queryForObject(TaskerQueries.GET_TASKS_BY_USER_ID_AND_TASK_ID, new Object[]{userId, taskId}, taskerMapper));
        } catch (IncorrectResultSizeDataAccessException exception) {
            return Optional.empty();
        }
    }

    @Override
    public Tasker update(Tasker task) {
        log.info("Updating task with id = {}", task.getId());
        int result = this.getJdbcTemplate().update(TaskerQueries.UPDATE_TASK,
                task.getTitle(),
                task.getStartDate(),
                task.getEndDate(),
                task.getDescription(),
                task.getPriority().getValue(),
                task.getId());
        if (result == 0) {
            throw new ResourceNotFoundException(Tasker.class.getSimpleName(), task.getId());
        } else {
            return this.findById(task.getId()).get();
        }
    }

    @Override
    public void delete(Long userId) {
        log.info("Removing all tasks for user id ={} from database", userId);
        this.getJdbcTemplate().update(TaskerQueries.DELETE_TASK_BY_USER_ID, userId);
    }


    @Override
    public Optional<Tasker> findById(Long taskId) {
        log.info("Retrieving a specific task = {}", taskId);
        try {
            return Optional.ofNullable(getJdbcTemplate().queryForObject(TaskerQueries.GET_TASK_BY_ID, new Object[]{taskId}, taskerMapper));
        } catch (IncorrectResultSizeDataAccessException exception) {
            return Optional.empty();
        }
    }

}
