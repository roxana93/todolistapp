package com.javachallenge.todolist.application.repository.impl;

import com.javachallenge.todolist.application.exception.RetryException;
import com.javachallenge.todolist.application.model.User;
import com.javachallenge.todolist.application.repository.UserRepository;
import com.javachallenge.todolist.application.repository.mapper.UserRowMapper;
import com.javachallenge.todolist.application.repository.utils.UserQueries;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.Optional;


@Repository
@Slf4j
public class UserRepositoryImpl extends JdbcDaoSupport implements UserRepository {

    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void initialize() {

        setDataSource(dataSource);
    }

    @Override
    public User save(User user) {
        try {
            getJdbcTemplate().update(UserQueries.INSERT_QUERY, user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword().toString());
            return user;
        } catch (CannotGetJdbcConnectionException exception) {
            throw new RetryException("Connection error ", exception);
        }
    }

    @Override
    public List<User> findAll() {
        log.info("Retrieving all users from database!");
        List<User> users = getJdbcTemplate().query(UserQueries.SELECT_ALL_USERS, new UserRowMapper());
        return users;
    }

    @Override
    public Optional<User> findById(Long id) {
        log.info("Retrieving user with id {}", id);
        try {
            User user = getJdbcTemplate().queryForObject(UserQueries.GET_USER_BY_ID, new Object[]{id}, new UserRowMapper());
            return Optional.ofNullable(user);
        } catch (DataAccessException exception) {
            log.error(exception.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public User update(User type) {
        return null;
    }

    @Override
    public void delete(Long userId) {
        log.info("Deleting user with id {}", userId);
        this.getJdbcTemplate().update(UserQueries.DELETE_USER_BY_ID,userId);
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        try {
            User actualUser = getJdbcTemplate().queryForObject(
                    UserQueries.GET_USER_BY_EMAIL,
                    new Object[]{email},
                    new UserRowMapper());
            return Optional.of(actualUser);
        } catch (DataAccessException exception) {
            log.error(exception.getMessage());
            return Optional.empty();
        }
    }


}
