package com.javachallenge.todolist.application.repository;

import com.javachallenge.todolist.application.model.Tasker;

import java.util.List;
import java.util.Optional;

public interface TaskerRepository extends Repository<Tasker, Long> {
    List<Tasker> findTasksByUserId(Long userId);

    Optional<Tasker> findTaskByUserIdAndTaskId(Long userId, Long taskId);
}
