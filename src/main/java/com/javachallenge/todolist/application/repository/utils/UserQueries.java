package com.javachallenge.todolist.application.repository.utils;

public class UserQueries {

    public static final String INSERT_QUERY = "INSERT INTO users (firstname, lastname, email, password) VALUES (?, ?, ?, ?);";
    public static final String SELECT_ALL_USERS = "SELECT * FROM users;";
    public static final String GET_USER_BY_ID = "SELECT * FROM users WHERE id = ?;";
    public static final String GET_USER_BY_EMAIL = "SELECT * FROM users WHERE email = ?;";
    public static final String DELETE_USER_BY_ID = "DELETE FROM users WHERE id=?;";
}
