package com.javachallenge.todolist.application.repository.utils;

public class TaskerQueries {
    public static final String CREATE_USER = "INSERT INTO tasker (title, startDate, endDate, description, priority, user_id) VALUES (?,?,?,?,?,?);";
    public static final String GET_ALL_TASKS = "SELECT * FROM tasker;";
    public static final String GET_TASK_BY_ID = "SELECT * FROM tasker  WHERE tasker_id=?;";
    public static final String UPDATE_TASK = "UPDATE tasker " +
            "SET title=?, startdate=?, enddate=?, description=?, priority=?" +
            "WHERE tasker_id=?;";
    public static final String DELETE_TASK_BY_USER_ID = "DELETE FROM todoapp.tasker WHERE user_id=?;";
    public static final String GET_TASKS_BY_USER_ID_AND_TASK_ID = "SELECT * FROM tasker  WHERE user_id=? AND tasker_id=?;";
    public static final String GET_TASKS_BY_USER_ID = "SELECT * FROM tasker  WHERE user_id=?;";
}
