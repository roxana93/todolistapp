package com.javachallenge.todolist.application.repository.mapper;

import com.javachallenge.todolist.application.exception.ResourceNotFoundException;
import com.javachallenge.todolist.application.model.Tasker;
import com.javachallenge.todolist.application.model.User;
import com.javachallenge.todolist.application.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;

@Slf4j
@Service
public class TaskerMapper implements RowMapper<Tasker> {

    private final UserRepository userRepository;

    @Autowired
    public TaskerMapper(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public Tasker mapRow(ResultSet resultSet, int rowNumber) throws SQLException {
        Tasker task = new Tasker();

        Tasker.Priority priority = Tasker.Priority.getInstance(resultSet.getString("priority"));
        Long userId = resultSet.getLong("user_id");
        User user = userRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(User.class.getSimpleName(), userId));

        task.setId(resultSet.getLong("tasker_id"));
        task.setStartDate(resultSet.getDate("startDate"));
        task.setEndDate(resultSet.getDate("endDate"));
        task.setDescription(resultSet.getString("description"));
        task.setPriority(priority);
        task.setUser(user);
        return task;
    }
}
