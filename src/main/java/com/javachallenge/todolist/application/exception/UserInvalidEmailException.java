package com.javachallenge.todolist.application.exception;

public class UserInvalidEmailException extends RuntimeException {

    public UserInvalidEmailException() {
    }

    public UserInvalidEmailException(String message) {
        super(message);
    }

    public UserInvalidEmailException(Throwable cause) {
        super(cause);
    }

    public UserInvalidEmailException(String message, Throwable cause) {
        super(message, cause);
    }
}
