package com.javachallenge.todolist.application.exception;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException() {
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }


    public ResourceNotFoundException(String entityClassName, Long id) {
        super(entityClassName + " with id " + id + " does not exist!");
    }


}
