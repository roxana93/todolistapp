package com.javachallenge.todolist.application.model;

import lombok.*;

import java.util.Date;

@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Tasker {
    private Long id;
    private String title;
    private Date startDate;
    private Date endDate;
    private String description;
    private Priority priority;
    private User user;

    public enum Priority {
        LOW("Low"),
        MEDIUM("Medium"),
        HIGH("High");

        private String value;

        private Priority(String value) {
            this.value = value;
        }

        public static Priority getInstance(String value) {
            if (value == null) {
                return null;
            }

            switch (value.toLowerCase()) {
                case "high":
                    return Priority.HIGH;
                case "medium":
                    return Priority.MEDIUM;
                default:
                    return Priority.LOW;
            }
        }

        public String getValue() {
            return value;
        }

        public String toString() {
            return this.value;
        }

    }

}
