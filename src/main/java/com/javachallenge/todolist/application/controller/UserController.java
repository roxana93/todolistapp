package com.javachallenge.todolist.application.controller;

import com.javachallenge.todolist.application.dto.UserLoginDto;
import com.javachallenge.todolist.application.model.User;
import com.javachallenge.todolist.application.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<User> createNewAccount(@Valid @RequestBody User user) {
        log.info("Post request for saving user with id: {}", user.getUserId());
        User userSaved = userService.save(user);
        return new ResponseEntity<>(userSaved, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        log.info("GET request to display all users from database!");
        List<User> userList = userService.findAll();
        return new ResponseEntity<>(userList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        log.info("Get request to display user informations for id: {}", id);
        Optional<User> user = userService.findById(id);
        return new ResponseEntity<>(user.get(), HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<HttpStatus> loginUser(@RequestBody UserLoginDto user) {
        log.info("Post request to login user with adresss email", user.getEmail());
        userService.login(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long userId) {
        log.info("DELETE user with id: {}", userId);
        userService.delete(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
