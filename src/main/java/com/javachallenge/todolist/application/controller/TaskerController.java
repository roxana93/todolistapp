package com.javachallenge.todolist.application.controller;

import com.javachallenge.todolist.application.model.Tasker;
import com.javachallenge.todolist.application.service.TaskerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/todo")
public class TaskerController {

    private TaskerService taskerService;

    @Autowired
    public TaskerController(TaskerService taskerService) {
        this.taskerService = taskerService;
    }

    @PostMapping
    public ResponseEntity<Tasker> createNewTask(@RequestBody Tasker task) {
        log.info("POST request for create a new task for user id: {}", task.getUser().getUserId());
        Tasker taskSaved = taskerService.save(task);
        return new ResponseEntity<>(taskSaved, HttpStatus.CREATED);
    }

    @GetMapping("all-tasks/{userId}")
    public ResponseEntity<List<Tasker>> getListOfAllTasksForTheCurrentUser(@PathVariable Long userId) {
        log.info("GET request to display all tasks for the user with id: {}", userId);
        List<Tasker> taskerList = taskerService.findTasksByUserId(userId);
        return new ResponseEntity<>(taskerList, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<HttpStatus> delete(@PathVariable Long userId) {
        log.info("DELETE request for user id: {}", userId);
        taskerService.delete(userId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{taskId}")
    public ResponseEntity<Tasker> update(@PathVariable Long taskId, @RequestBody Tasker task) {
        log.info("PUT request for updating task with id: {}", taskId);
        task.setId(taskId);
        Tasker updatedTask = taskerService.update(task);
        return new ResponseEntity<>(updatedTask, HttpStatus.OK);
    }

}
