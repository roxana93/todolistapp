package com.javachallenge.todolist.application.service;

import com.javachallenge.todolist.application.model.Tasker;

import java.util.List;

public interface TaskerService extends Service<Tasker, Long> {
    List<Tasker> findTasksByUserId(Long userId);
}
