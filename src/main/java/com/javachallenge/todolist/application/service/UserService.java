package com.javachallenge.todolist.application.service;

import com.javachallenge.todolist.application.dto.UserLoginDto;
import com.javachallenge.todolist.application.model.User;

public interface UserService extends Service<User, Long> {
    Boolean userEmailAlreadyExist(User user);

    String login(UserLoginDto userLoginDto);
}
