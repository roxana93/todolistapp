package com.javachallenge.todolist.application.service.impl;

import com.javachallenge.todolist.application.dto.UserLoginDto;
import com.javachallenge.todolist.application.exception.IncorrectPasswordException;
import com.javachallenge.todolist.application.exception.ResourceNotFoundException;
import com.javachallenge.todolist.application.exception.UserInvalidEmailException;
import com.javachallenge.todolist.application.model.User;
import com.javachallenge.todolist.application.repository.UserRepository;
import com.javachallenge.todolist.application.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User save(User user) {
        if (!this.userEmailAlreadyExist(user)) {
            User userSaved = userRepository.save(user);
            return userSaved;
        } else {
            throw new UserInvalidEmailException("The user with email address: " + user.getEmail() + " already exists!");
        }
    }

    @Override
    public List<User> findAll() {
        log.info("Retrieving all users!");
        List<User> userList = userRepository.findAll();
        return userList;
    }

    @Override
    public Optional<User> findById(Long id) {
        Optional<User> actualUser = userRepository.findById(id);

        if (actualUser.isPresent()) {
            log.info("The user with id " + actualUser.get().getUserId() + " exists!");
            return actualUser;
        } else {
            log.warn("The user with id " + id + " could not be found!");
            throw new ResourceNotFoundException("The user with id " + id + " could not be found!");
        }
    }

    @Override
    public User update(User type) {
        return null;
    }

    @Override
    public void delete(Long userId) {
        log.info("Deleting user with id {}", userId);
        userRepository.delete(userId);
    }

    @Override
    public Boolean userEmailAlreadyExist(User user) {
        Optional<User> actualUser = userRepository.getUserByEmail(user.getEmail());

        if (actualUser.isPresent()) {
            log.warn("The user with email address " + user.getEmail() + " already exist!");
            throw new UserInvalidEmailException("The user with email address: " + user.getEmail() + " already exist!");
        } else {
            log.info("The user with email address " + user.getEmail() + " could not be found!");
            return false;
        }
    }

    @Override
    public String login(UserLoginDto userLoginDto) {
        try {
            Optional<User> user = userRepository.getUserByEmail(userLoginDto.getEmail());

            if (Arrays.equals(user.get().getPassword(), userLoginDto.getPassword())) {
                return "Login Succesfully!";
            } else
                throw new IncorrectPasswordException();
        } catch (NullPointerException exception) {
            throw new UserInvalidEmailException("Email " + userLoginDto.getEmail() + " not found!", exception);
        }
    }


}
