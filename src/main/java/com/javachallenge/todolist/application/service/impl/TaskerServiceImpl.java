package com.javachallenge.todolist.application.service.impl;

import com.javachallenge.todolist.application.model.Tasker;
import com.javachallenge.todolist.application.repository.TaskerRepository;
import com.javachallenge.todolist.application.service.TaskerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TaskerServiceImpl implements TaskerService {

    private TaskerRepository taskerRepository;

    @Autowired
    public TaskerServiceImpl(TaskerRepository taskerRepository) {
        this.taskerRepository = taskerRepository;
    }


    @Override
    public Tasker save(Tasker task) {
        return taskerRepository.save(task);
    }

    @Override
    public List<Tasker> findAll() {
        log.info("Retrieving task list");
        List<Tasker> taskList = taskerRepository.findAll();
        return taskList;
    }

    @Override
    public List<Tasker> findTasksByUserId(Long userId) {
        log.info("Find all tasks for the current user {}", userId);
        List<Tasker> taskList = taskerRepository.findTasksByUserId(userId);
        return taskList;
    }

    @Override
    public Tasker update(Tasker task) {
        log.info("Updating task with id {}", task.getId());
        Tasker tasker = taskerRepository.update(task);
        return tasker;
    }

    @Override
    public void delete(Long userId) {
        log.info("Deleting all tasks for user = {}", userId);
        taskerRepository.delete(userId);
    }

    @Override
    public Optional<Tasker> findById(Long id) {
        return Optional.empty();
    }

}
