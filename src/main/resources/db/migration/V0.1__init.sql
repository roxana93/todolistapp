CREATE TABLE IF NOT EXISTS users (
    id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
    firstName VARCHAR(100) NOT NULL,
    lastName VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL UNIQUE,
    password character varying NOT NULL,
    PRIMARY KEY(id)
);


CREATE TABLE IF NOT EXISTS tasker (
    tasker_id BIGINT GENERATED ALWAYS AS IDENTITY NOT NULL,
    title VARCHAR(100) NOT NULL,
    startDate DATE NOT NULL,
    endDate DATE NOT NULL,
    description TEXT NOT NULL,
    priority VARCHAR(20) NOT NULL,
    user_id BIGINT REFERENCES users(id),
    PRIMARY KEY(tasker_id)
);